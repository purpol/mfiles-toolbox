# M-Files Toolbox Console #

This application is built for bulk user account and login account operations. List of available operations differs for User accounts and login accounts.


## User Account Operations ##

- Bulk enable/disable user accounts
- Bulk mark external / mark internal user accounts.
- Add/Remove roles such as "Manage Common Views", "Can See All Objects" etc.
- Bulk remove user accounts

## Login Account Operations ##

- Bulk enable/disable login accounts
- Bulk remove login accounts

## Options / Arguments ##

**-i (Interface)(Required): **  
This option is for choosing interface. Available interfaces are "userops" and "loginops".

** -v (Vault)(Required): **  
Argument for specifying vault for operations.  

** -s (Server)(optional): **  
This argument is for specifying server address for connecting.  

**  -p (Protocol)(optional): **  
Connection protocol that the target server uses. By default this option is set to "ncacn\_ip\_tcp" can be altered with one of the following "ncacn\_spx", "ncalrpc" or "ncacn\_http"  

** -E (Endpoint)(optional) **  
Endpoint for server address. By defeult this option is set to "2266".  

** -U (Username)(optional) **  
Username for connecting to the given vault. You **must** provide this and argument and password argument if the user running this application does not have administrative rights on target M-Files Server.  

** -P (Password)(optional) **  
Password for connecting to the given vault. You **must** provide this and argument and password argument if the user running this application does not have administrative rights on target M-Files Server.  

** -t (Usertype)(optional) **  
Usertype can be specified for connection. By default this is "LoggedOn" user but "MFiles" and "Windows" options can be used.  


** ? (Help) **  
Help documentation.  

** -r (Remove) **  
Removes users/login accounts from the given vault. Option **all** or **-all** can be used to remove all the users/login accounts.  

** -d (Disable) **  
Disables users/login accounts at the given vault. Option **all** or **-all** can be used to disable all the users/login accounts.  

** -e (Enable) **  
Enables users/login accounts at the given vault. Option **all** or **-all** can be used to enable all the users/login accounts.  

** -m (Mark External) **  
Marks users at the given vault as external user. Option **all** or **-all** can be used to mark all the user accounts at once.  

** -n (Mark Internal) **  
Marks users at the given vault as internal user. Option **all** or **-all** can be used to mark all the user accounts at once.  

** -A (Add Role) **  
Adds the given role to the users. Available roles can be seen at [https://www.m-files.com/api/documentation/latest/index.html#MFilesAPI~MFUserAccountVaultRole.html](https://www.m-files.com/api/documentation/latest/index.html#MFilesAPI~MFUserAccountVaultRole.html "User Account Roles") Option -u **must** must be used to specify users for operation **all** or **-all** parameters can be used to add roles to all users at once.  

** -K (Remove Role) **  
Removes the given role to the users. Available roles can be seen at [https://www.m-files.com/api/documentation/latest/index.html#MFilesAPI~MFUserAccountVaultRole.html](https://www.m-files.com/api/documentation/latest/index.html#MFilesAPI~MFUserAccountVaultRole.html "User Account Roles") Option -u **must** must be used to specify users for operation **all** or **-all** parameters can be used to add roles to all users at once.  

** -x (Exclude) **  
Argument for excluding users from bulk operations.  

## Examples ##


**Remove Traditional Folder Management Role**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -K "2048" -u "all"

**Disable All Users**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -d "all"  

**Disable All Users Except Some**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -d "all" -x "24" "25"

**Disable All Login Accounts**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "loginops" -d "all"  

**Mark given users as external**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -m "24" "25" "27" "28"

**Remove User Accounts**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -r "24" "25" "27" "28"

**Remove All User Accounts**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -r "all"

**Remove All User Accounts Except Some**  
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "userops" -r "all" -x "24" "25" "26"

**Remove All Login Accounts**
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "loginops" -r "all"  

**Remove Selected Login Accounts**
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "loginops" -r "Ayse" "Fatma" "Hayriye"

**Remove All Login Accounts Except Some**
>./Toolbox.exe -v "{5CD7CA26-F23F-4A75-BE5C-A29C1F7B5055}" -i "loginops" -r "all" -x "Ayse" "Fatma" "Hayriye"