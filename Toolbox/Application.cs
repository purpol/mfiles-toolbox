﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Toolbox.Program;

namespace Toolbox
{
    public class Application
    {
        static IMFilesHelper _helper;

        public static void Run(ApplicationArguments args)
        {


            var ids = String.Join(",", args.Ids.ToArray());

            //Give Parameters
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Given Arguments");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine($"ServerAddress: {args.ServerAddress}");
            Console.WriteLine($"Interface: {args.Interface}");
            Console.WriteLine($"UserIds: {String.Join(",", args.Ids.ToArray())}");
            Console.WriteLine($"Excluded Users: " + args.ExcludedIds != null ? String.Join(",", args.Ids.ToArray()) : "");
            Console.WriteLine($"Protocol: {args.Protocol}");
            Console.WriteLine($"Endpoint: {args.EndPoint}");
            Console.WriteLine($"RoleIds: {args.RoleId}");
            Console.WriteLine($"SubOperation: {args.SubOp}");
            Console.WriteLine($"Username: {args.Username}");
            Console.WriteLine($"Password: {args.Password}");
            Console.WriteLine($"UserType: {args.UserType}");
            Console.WriteLine($"Vault: {args.VaultGuid}");

            Console.WriteLine("Is it ok to start the job with the given parameters?");
            Console.WriteLine("[Y]es or [n]o ?");

            if (args.PreConfirm)
            {
                Application._Run_Safe(args);
                return;
            }

            var keyInfo = Console.ReadKey();

            if (keyInfo.Key != ConsoleKey.Y && keyInfo.Key != ConsoleKey.N)
            {
                Application.Run(args);
            }

            if (keyInfo.Key == ConsoleKey.Y)
            {
                _Run_Safe(args);

            }


        }

        public static void _Run_Safe(ApplicationArguments args)
        {
            Console.WriteLine("Starting operation.");

            _helper = new MFilesHelper();
            _helper.Connect(args.ServerAddress, args.Username, args.Password, args.EndPoint, "", args.Protocol, args.UserType);
            Console.WriteLine("Connected to server.");

            _helper.LoginToVault(args.VaultGuid);
            Console.WriteLine("Logged into vault.");


            if (args.Interface == "userops")
            {
                switch (args.SubOp)
                {
                    case SubOperationType.Remove:
                        _helper.RemoveUsers(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.AddRole:
                        _helper.AddRole(args.RoleId, args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.RemoveRole:
                        _helper.RemoveRole(args.RoleId, args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.Disable:
                        _helper.DisableUsers(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.MarkExternal:
                        _helper.MarkExternal(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.MarkInternal:
                        _helper.MarkInternal(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.Enable:
                        _helper.EnableUsers(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    default:
                        Console.Write($"{args.SubOp} is not available for userops interface");
                        return;
                }
            }
            else if (args.Interface == "loginops")
            {
                switch (args.SubOp)
                {
                    case SubOperationType.Remove:
                        _helper.RemoveLogins(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.Disable:
                        _helper.DisableLoginAccounts(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    case SubOperationType.Enable:
                        _helper.EnableLoginAccounts(args.Ids.ToArray(), args.ExcludedIds?.ToArray());
                        break;
                    default:
                        Console.Write($"{args.SubOp} is not available for loginops interface");
                        return;
                }
            }
            else
            {
                Console.WriteLine($"Unknown interface {args.Interface}");
                return;
            }

        }
    }
}
