﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox
{
    public class ApplicationArguments
    {
        public string Interface { get; set; }
        public List<string> Ids { get; set; }
        public List<string> ExcludedIds { get; set; }
        public SubOperationType SubOp { get; set; }
        public string ServerAddress { get; set; }
        public string VaultGuid { get; set; }
        public string Protocol { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EndPoint { get; set; }
        public UserType UserType { get; set; }
        public int RoleId { get; set; }
        public bool PreConfirm { get; set; }
    }
}
