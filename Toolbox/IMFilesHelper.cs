﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox
{
    public interface IMFilesHelper
    {

        //Connect
        void ConnectAdministrative();
        void Connect(string serveraddress = null, string username = null, string password = null, string endpoint = null, string domain = null, string protocol = null, UserType userType = UserType.LoggedOn);
        void LoginToVault(string vaultGuid);

        //Remove Users
        void RemoveUsers(string[] userIds, string [] exceptUsers = null);

        //Remove Login Accounts
        void RemoveLogins(string[] userIds, string[] exceptUsers = null);

        //Disable Users
        void DisableUsers(string[] userIds, string[] exceptUsers = null);

        //Enable Users
        void EnableUsers(string[] userIds, string[] exceptUsers = null);

        //Disable LoginAccounts
        void DisableLoginAccounts(string[] userIds, string[] exceptUsers = null);

        //Enable Login Accounts
        void EnableLoginAccounts(string[] userIds, string[] exceptUsers = null);

        //Add Role
        void AddRole(int roleId, string[] userIds, string[] exceptUsers = null);

        //Remove Role
        void RemoveRole(int roleId, string[] userIds, string[] exceptUsers = null);

        //Mark External
        void MarkExternal(string[] userIds, string[] exceptUsers = null);

        //Mark Internal
        void MarkInternal(string[] userIds, string[] exceptUsers = null);

    }
}
