﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFilesAPI;
using System.Runtime.InteropServices;

namespace Toolbox
{
    public class MFilesHelper : IMFilesHelper
    {

        MFilesServerApplication _serverApp;
        Vault _loggedInVault;

        public MFilesHelper()
        {
            _serverApp = new MFilesServerApplication();
        }

        public void AddRole(int roleId, string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];

            if (zeroIndex == "all" || zeroIndex == "-all")
            {
                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Add role operation started for {users.Count} user(s).");

                foreach (UserAccount item in users)
                {
                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        } 
                    }

                    Console.WriteLine($"Adding role to {item.LoginName}.");
                    item.AddVaultRoles((MFUserAccountVaultRole)roleId);

                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));

                    Console.WriteLine($"Adding role to {userAccount.LoginName}.");
                    userAccount.AddVaultRoles((MFUserAccountVaultRole)roleId);

                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void Connect(string serveraddress = null, string username = null, string password = null, string endpoint = null, string domain = null, string protocol = null, UserType userType = UserType.LoggedOn)
        {
            MFAuthType authType;

            switch (userType)
            {
                case UserType.MFiles:
                    authType = MFAuthType.MFAuthTypeSpecificMFilesUser;
                    break;
                case UserType.Windows:
                    authType = MFAuthType.MFAuthTypeSpecificWindowsUser;
                    break;
                case UserType.LoggedOn:
                    authType = MFAuthType.MFAuthTypeLoggedOnWindowsUser;
                    break;
                default:
                    authType = MFAuthType.MFAuthTypeLoggedOnWindowsUser;
                    break;
            }

            _serverApp.Connect(authType, username, password, domain, protocol, serveraddress, endpoint);

        }

        public void ConnectAdministrative()
        {
            _serverApp.ConnectAdministrativeEx();
        }

        public void DisableLoginAccounts(string[] userIds, string[] exceptUsers = null)
        {
            var zeroIndex = userIds[0];

            if (zeroIndex == "all" || zeroIndex == "-all")
            {
                var users = _serverApp.LoginAccountOperations.GetLoginAccounts();

                Console.WriteLine($"Disable login account operation started for {users.Count} user(s).");

                foreach (LoginAccount item in users)
                {
                    if (!item.Enabled)
                    {
                        Console.WriteLine($"Login account with name: {item.AccountName} is already disabled, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.AccountName))
                        {
                            continue;
                        } 
                    }


                    Console.WriteLine($"Disabling {item.AccountName}.");

                    item.Enabled = false;

                    _serverApp.LoginAccountOperations.ModifyLoginAccount(item);
                }
                return;
            }


            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                LoginAccount userAccount;
                try
                {
                    userAccount = _serverApp.LoginAccountOperations.GetLoginAccount(item);

                    if (!userAccount.Enabled)
                    {
                        Console.WriteLine($"Login account name: {userAccount.AccountName} is already disabled, skipped.");
                        continue;
                    }

                    Console.WriteLine($"Disabling {userAccount.AccountName}.");
                    userAccount.Enabled = false;
                    _serverApp.LoginAccountOperations.ModifyLoginAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void DisableUsers(string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];



            if (zeroIndex == "all" || zeroIndex == "-all")
            {
                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Disable user account operation started for {users.Count} user(s).");
                foreach (UserAccount item in users)
                {

                    if (!item.Enabled)
                    {
                        Console.WriteLine($"User with name: {item.LoginName} is already disabled, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Disabling {item.LoginName}.");
                    item.Enabled = false;
                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));

                    if (!userAccount.Enabled)
                    {
                        Console.WriteLine($"User with name: {userAccount.LoginName} is already disabled, skipped.");
                        continue;
                    }

                    Console.WriteLine($"Disabling {userAccount.LoginName}.");
                    userAccount.Enabled = false;
                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void LoginToVault(string vaultGuid)
        {
            _loggedInVault = _serverApp.LogInToVault(vaultGuid);
        }

        public void MarkExternal(string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {

                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Mark external operation startted for {users.Count} user(s).");
                foreach (UserAccount item in users)
                {

                    if (!item.InternalUser)
                    {
                        Console.WriteLine($"User with id:{item.LoginName} is already marked as external, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Marking {item.LoginName} as external.");
                    item.InternalUser = false;
                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));

                    if (!userAccount.InternalUser)
                    {
                        Console.WriteLine($"User with name:{userAccount.LoginName} is already marked as external, skipped.");
                        continue;
                    }


                    Console.WriteLine($"Marking {userAccount.LoginName} as external.");
                    userAccount.InternalUser = false;
                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void MarkInternal(string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {

                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Mark internal operation started for {users.Count} user(s).");
                foreach (UserAccount item in users)
                {

                    if (item.InternalUser)
                    {
                        Console.WriteLine($"User with id:{item.LoginName} is already marked as internal, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Marking {item.LoginName} as internal.");
                    item.InternalUser = true;
                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));

                    if (userAccount.InternalUser)
                    {
                        Console.WriteLine($"User with name:{userAccount.LoginName} is already marked as internal, skipped.");
                        continue;
                    }


                    Console.WriteLine($"Marking {userAccount.LoginName} as internal.");
                    userAccount.InternalUser = true;
                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void RemoveLogins(string[] userIds, string[] exceptUsers = null)
        {

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {
                var users = _serverApp.LoginAccountOperations.GetLoginAccounts();

                Console.WriteLine($"Remove operation started for {users.Count} accounts(s).");
                foreach (LoginAccount item in users)
                {

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.AccountName))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Removing {item.AccountName}.");
                    _serverApp.LoginAccountOperations.RemoveLoginAccount(item.AccountName);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                LoginAccount userAccount;
                try
                {
                    userAccount = _serverApp.LoginAccountOperations.GetLoginAccount(item);
                    Console.WriteLine($"Removing login account with name: {userAccount.AccountName}");

                    _serverApp.LoginAccountOperations.RemoveLoginAccount(userAccount.AccountName);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void RemoveRole(int roleId, string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {
                if (_loggedInVault == null)
                {
                    throw new Exception("Not logged in to M-Files");
                }

                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Remove role operation started for {users.Count} user(s).");

                foreach (UserAccount item in users)
                {

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Removing role from {item.LoginName}.");
                    item.RemoveVaultRoles((MFUserAccountVaultRole)roleId);

                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));

                    Console.WriteLine($"Removing role from {userAccount.LoginName}.");
                    userAccount.RemoveVaultRoles((MFUserAccountVaultRole)roleId);

                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void RemoveUsers(string[] userIds, string[] exceptUsers = null)
        {
            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {
                if (_loggedInVault == null)
                {
                    throw new Exception("Not logged in to M-Files");
                }

                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Remove operation started for {users.Count} user(s).");
                foreach (UserAccount item in users)
                {

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Removing {item.LoginName}.");

                    _loggedInVault.UserOperations.RemoveUserAccount(item.ID);
                }
                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));
                    Console.WriteLine($"Removing user with Id: {item}");

                    _loggedInVault.UserOperations.RemoveUserAccount(userAccount.ID);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void EnableUsers(string[] userIds, string[] exceptUsers = null)
        {

            if (_loggedInVault == null)
            {
                throw new Exception("Not logged in to M-Files");
            }

            var zeroIndex = userIds[0];
            if (zeroIndex == "all")
            {
                if (_loggedInVault == null)
                {
                    throw new Exception("Not logged in to M-Files");
                }

                var users = _loggedInVault.UserOperations.GetUserAccounts();

                Console.WriteLine($"Enable operation started for {users.Count} user(s).");
                foreach (UserAccount item in users)
                {
                    if (item.Enabled)
                    {
                        Console.WriteLine($"User with id:{item.ID} is already enabled, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.ID.ToString()))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Enabling {item.LoginName}.");
                    item.Enabled = true;
                    _loggedInVault.UserOperations.ModifyUserAccount(item);
                }

                return;
            }


            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                UserAccount userAccount;
                try
                {
                    userAccount = _loggedInVault.UserOperations.GetUserAccount(Convert.ToInt32(item));
                    Console.WriteLine($"Found user with Id: {item}");

                    if (userAccount.Enabled)
                    {
                        Console.WriteLine($"User with id:{item} is already enabled, skipped.");
                        continue;
                    }


                    userAccount.Enabled = true;
                    _loggedInVault.UserOperations.ModifyUserAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }
            }
        }

        public void EnableLoginAccounts(string[] userIds, string[] exceptUsers = null)
        {

            var zeroIndex = userIds[0];

            if (zeroIndex == "all")
            {
                var users = _serverApp.LoginAccountOperations.GetLoginAccounts();

                Console.WriteLine($"Enable login account operation started for {users.Count} user(s).");

                foreach (LoginAccount item in users)
                {
                    if (item.Enabled)
                    {
                        Console.WriteLine($"Login account with name: {item.AccountName} is already enabled, skipped.");
                        continue;
                    }

                    if (exceptUsers != null)
                    {
                        if (exceptUsers.Contains(item.AccountName))
                        {
                            continue;
                        }
                    }

                    Console.WriteLine($"Enabling {item.AccountName}.");

                    item.Enabled = true;

                    _serverApp.LoginAccountOperations.ModifyLoginAccount(item);
                }
                return;
            }

            foreach (var item in exceptUsers == null ? userIds : userIds.Except(exceptUsers))
            {
                LoginAccount userAccount;
                try
                {
                    userAccount = _serverApp.LoginAccountOperations.GetLoginAccount(item);

                    if (userAccount.Enabled)
                    {
                        Console.WriteLine($"Login account name: {userAccount.AccountName} is already enabled, skipped.");
                        continue;
                    }

                    Console.WriteLine($"Enabling {userAccount.AccountName}.");
                    userAccount.Enabled = true;
                    _serverApp.LoginAccountOperations.ModifyLoginAccount(userAccount);

                }
                catch (COMException ex)
                {
                    Console.WriteLine($"Got an error from M-Files while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message.Substring(0, ex.Message.IndexOf('\r'))}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Got an unhandled error while trying to update user with id: {item}");
                    Console.WriteLine($"Exception Message: {ex.Message}");
                }


            }
        }

    }
}
