﻿using Fclp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox
{
    class Program
    {

        static string[] subops = new string[] {
            "-K",
            "-r",
            "-A",
            "-m",
            "-d",
            "-e",
            "-n"
        };

        static void Main(string[] args)
        {
            var p = new FluentCommandLineParser<ApplicationArguments>();

            p.IsCaseSensitive = true;

            var suboperations = 0;

            foreach (var item in args)
            {
                if (subops.Contains(item))
                {
                    suboperations++;

                    if (item == "-R" || item == "-A")
                    {
                        if (!args.Contains("-u"))
                        {
                            Console.WriteLine("-u parameter is needed when using -A or -R parameters.");
                            return;
                        }
                    }
                }
            }

            if (suboperations > 1 && args[0] != "?")
            {
                Console.WriteLine("Multiple suboperations detected.");
                return;
            }

            if (suboperations == 0 && args[0] != "?")
            {
                Console.WriteLine("No suboperations.");
                return;
            }



            p.Setup(arg => arg.Interface)
             .As('i', "interface")
             .Required().WithDescription("Interface to work with.Available interfaces:userops,loginops");

            p.Setup(arg => arg.VaultGuid)
             .As('v', "guid")
             .Required()
             .WithDescription("Target vaults guid. Curly bracers '{' are needed.");


            p.Setup(arg => arg.Ids)
             .As('r', "remove")
             .Callback(val =>
             {
                 p.Object.Ids = val;
                 p.Object.SubOp = SubOperationType.Remove;
             }).WithDescription("Remove users/login accounts from the given vault. -all can be used to remove all the users/login accounts.");

            p.Setup(arg => arg.RoleId)
             .As('K', "removerole")
             .Callback(val =>
             {
                 p.Object.RoleId = val;
                 p.Object.SubOp = SubOperationType.RemoveRole;
             })
             .WithDescription("Removes the spesified role from the given users. Must be used with -u parameter. -all can be used to remove roles from all users.");

            p.Setup(arg => arg.RoleId)
             .As('A', "addrole")
             .Callback(val =>
             {
                 p.Object.RoleId = val;
                 p.Object.SubOp = SubOperationType.AddRole;
             })
             .WithDescription("Adds the spesified role to the given users. Must be used with -u parameter. -all can be used to add roles to all users.");

            p.Setup(arg => arg.Ids)
             .As('d', "disable")
             .Callback(val =>
             {
                 p.Object.Ids = val;
                 p.Object.SubOp = SubOperationType.Disable;
             }).WithDescription("Disables users at the given vault. -all can be used to disable all the users.");
            p.Setup(arg => arg.Ids)
             .As('e', "enable")
             .Callback(val =>
             {
                 p.Object.Ids = val;
                 p.Object.SubOp = SubOperationType.Enable;
             }).WithDescription("Enables users at the given vault. -all can be used to enable all the users.");

            p.Setup(arg => arg.Ids)
             .As('u', "users")
             .Callback(val =>
             {
                 p.Object.Ids = val;
             }).WithDescription("Must be used with -A and -R parameters. -all can be used to effect all users.");

            p.Setup(arg => arg.ExcludedIds)
             .As('x', "exclude")
             .Callback(val =>
             {
                 p.Object.ExcludedIds = val;
             }).WithDescription("Can be used to exclude users from operations.");

            p.Setup(arg => arg.Ids)
             .As('m', "markext")
             .Callback(val =>
             {
                 p.Object.Ids = val;
                 p.Object.SubOp = SubOperationType.MarkExternal;
             }).WithDescription("Marks the given users as external users. -all parameter can be used to mark all the users as external.");

            p.Setup(arg => arg.Ids)
             .As('n', "markint")
             .Callback(val =>
             {
                 p.Object.Ids = val;
                 p.Object.SubOp = SubOperationType.MarkInternal;
             }).WithDescription("Marks the given users as internal users. -all parameter can be used to mark all the users as internal at once.");

            p.Setup(arg => arg.ServerAddress)
             .As('s', "server")
             .SetDefault("localhost").
             WithDescription("Server address for connecting servers other then localhost.");

            p.Setup(arg => arg.Protocol)
             .As('p', "protocol")
             .SetDefault("ncacn_ip_tcp")
             .WithDescription("Default connection protocol can be altered with ncacn_spx, ncalrpc or ncacn_http protocols.");

            p.Setup(arg => arg.EndPoint)
             .As('E', "endpoint")
             .SetDefault("2266")
             .WithDescription("Default endpoint can be altered with the target servers required endpoint.");

            p.Setup(arg => arg.Username)
             .As('U', "username")
             .SetDefault(null)
             .WithDescription("Username to connect to the server. Can be null if the account running this application can connect with administrative rights to M-Files.");

            p.Setup(arg => arg.Password)
             .As('P', "password")
             .SetDefault(null);

            p.Setup(arg => arg.UserType)
             .As('t', "usertype")
             .SetDefault(UserType.LoggedOn)
             .WithDescription("Default account type for connection. MFiles and Windows options can be used.");

            p.Setup(arg => arg.PreConfirm)
                .As('c', "preconfirm")
                .SetDefault(false)
                .WithDescription("Preconfirm the confirmation page");


            var result = p.Parse(args);

            p.SetupHelp("?", "help")
      .Callback(text => Console.WriteLine(text));

            if (args[0] == "?")
            {
                p.HelpOption.ShowHelp(p.Options);
                return;
            }


            if (result.HasErrors)
            {
                Console.WriteLine(result.ErrorText);
                return;
            }

            try
            {
                Application.Run(p.Object);
            }
            catch (COMException Ex)
            {

                Console.WriteLine($"M-Files threw an exception: {Ex.Message.Substring(0, Ex.Message.IndexOf('\r'))}");
            }
            catch (Exception Ex)
            {

                Console.WriteLine($"Unhandled exception occured: {Ex.Message}");
            }
        }
    }
}
