﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Toolbox
{
    public enum SubOperationType
    {
        Remove = 1,
        AddRole = 2,
        RemoveRole = 3,
        Disable = 4,
        MarkExternal = 5,
        MarkInternal = 7,
        Enable = 6
    }
}
